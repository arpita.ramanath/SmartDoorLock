/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief This is the application entry point.
 * 			FreeRTOS and stdio printf is pre-configured to use uart0_min.h before main() enters.
 * 			@see L0_LowLevel/lpc_sys.h if you wish to override printf/scanf functions.
 *
 */
#include "tasks.hpp"
#include "examples/examples.hpp"
#include "uart2.hpp"
#include "utilities.h"
#include <stdio.h>
#include "string.h"
#include <stdlib.h>
#include <math.h>
//#include "font.h"
#include "ssp1.h"
#include <LCD_uart.hpp>
#include "LPC17xx.h"

/**
 * The main() creates tasks or "threads".  See the documentation of scheduler_task class at scheduler_task.hpp
 * for details.  There is a very simple example towards the beginning of this class's declaration.
 *
 * @warning SPI #1 bus usage notes (interfaced to SD & Flash):
 *      - You can read/write files from multiple tasks because it automatically goes through SPI semaphore.
 *      - If you are going to use the SPI Bus in a FreeRTOS task, you need to use the API at L4_IO/fat/spi_sem.h
 *
 * @warning SPI #0 usage notes (Nordic wireless)
 *      - This bus is more tricky to use because if FreeRTOS is not running, the RIT interrupt may use the bus.
 *      - If FreeRTOS is running, then wireless task may use it.
 *        In either case, you should avoid using this bus or interfacing to external components because
 *        there is no semaphore configured for this bus and it should be used exclusively by nordic wireless.
 */
Uart2& uart2 = Uart2::getInstance();
void uart2putch(char out)
{
    LPC_UART2->THR = out;
    while(! BIT(LPC_UART2->LSR).b6);
}
void uart2putstr(char c_string[])
{
    char* p = (char*) c_string;
    while(*p)
    {
        uart2putch(*p);
        p++;
    }
}

void btinit()
{

      uart2.init(38400,1000,1000);

      delay_ms(1000);
      uart2putstr("\r\n+STWMOD=0\r\n");
      uart2putstr("\r\n+STNA=Athavan-st\r\n");
      uart2putstr("\r\n+STAUTO=0\r\n");
      uart2putstr("\r\n+STOAUT=1\r\n");
      uart2putstr("\r\n +STPIN=0000\r\n");
      delay_ms(2000); // This delay is required.
      uart2putstr("\r\n+INQ=1\r\n");
      delay_ms(2000); // This delay is required.

}


char *get,*get1, receive1[4],receive[4];
void BT_function()
{

          int temp_lock=0, temp_open=0, temp_pin=0;
          char arr1[4],arr2[]="lock", arr3[]="open", pin[]="1234";
           for(int i=0;i<4;i++)
        {
               arr1[i]=receive[i];
        }
           for(int i=0;i<4;i++)
                 {
                        if(arr1[i]==arr2[i])
                        {
                            temp_lock++;
                        }
                        else if(arr1[i]==arr3[i])
                        {
                             temp_open++;
                        }

                  }
           if(temp_lock==4)
           {
               //do something to lock the gate
               printf("door is locked \n");
               uart2putstr("\r\n door is locked \r\n");

           }
           else if(temp_open==4)
           {
                        //do something to lock the gate
                uart2putstr("\r\n password?\r\n");
                {
                    get1= (char*)receive1;
                    uart2.gets(get1,4, portMAX_DELAY);

                    for(int j=0;j<4;j++)
                   {
                       if(receive1[j]==pin[j])
                       {
                           temp_pin++;
                       }
                   }
                   if(temp_pin==4)
                   {
                       //do something to open the door
                       printf("door is open \n");
                       uart2putstr("\r\n door is open \r\n");
                   }
                   else{

                       uart2putstr("\r\n check password \r\n");
                   }

                }

           }
           else{

               uart2putstr("\r\n enter 'open' or 'lock'\r\n");
               //printf("enter 'open' or 'lock' \n");
           }
           temp_open=0;
           temp_lock=0;
           temp_pin=0;
 }
class BT_Task : public scheduler_task
{
    public:
        BT_Task(uint8_t priority) :
            scheduler_task("BT_Task", 2048, priority)
        {
            /* Nothing to init */
        }

        bool run(void *p)
        {

            //char* p = (char*) c_string;
            get= (char*)receive;
            uart2.gets(get,4, portMAX_DELAY);
            BT_function();
            for(int i=0;i<4;i++)
        {

        }
            printf("\n");
            return true;
        }
};


class LCDTask : public scheduler_task
{
    public:
        LCDTask(uint8_t priority) :
            scheduler_task("LCDTask", 15000, priority)
        {
            //LCD_Display();
            /* Nothing to init */
        }

        bool run(void *p)
        {
            Uart_LCD();
            vTaskDelay(3000); //LCD_Display();
            return true;
        }
};

int main(void)
{
    /**
     * A few basic tasks for this bare-bone system :
     *      1.  Terminal task provides gateway to interact with the board through UART terminal.
     *      2.  Remote task allows you to use remote control to interact with the board.
     *      3.  Wireless task responsible to receive, retry, and handle mesh network.
     *
     * Disable remote task if you are not using it.  Also, it needs SYS_CFG_ENABLE_TLM
     * such that it can save remote control codes to non-volatile memory.  IR remote
     * control codes can be learned by typing the "learn" terminal command.
     */
    scheduler_add_task(new terminalTask(PRIORITY_HIGH));
    btinit();
    scheduler_add_task(new BT_Task(PRIORITY_MEDIUM));
    scheduler_add_task(new terminalTask(PRIORITY_HIGH));
    //scheduler_add_task(new Door_fun(PRIORITY_MEDIUM));

    /* Consumes very little CPU, but need highest priority to handle mesh network ACKs */
   scheduler_add_task(new wirelessTask(PRIORITY_CRITICAL));

    /* Change "#if 0" to "#if 1" to run period tasks; @see period_callbacks.cpp */
    #if 0
    scheduler_add_task(new periodicSchedulerTask());
    #endif

    /* The task for the IR receiver */
    // scheduler_add_task(new remoteTask  (PRIORITY_LOW));

    /* Your tasks should probably used PRIORITY_MEDIUM or PRIORITY_LOW because you want the terminal
     * task to always be responsive so you can poke around in case something goes wrong.
     */

    /**
     * This is a the board demonstration task that can be used to test the board.
     * This also shows you how to send a wireless packets to other boards.
     */
    #if 0
        scheduler_add_task(new example_io_demo());
    #endif

    /**
     * Change "#if 0" to "#if 1" to enable examples.
     * Try these examples one at a time.
     */
    #if 0
        scheduler_add_task(new example_task());
        scheduler_add_task(new example_alarm());
        scheduler_add_task(new example_logger_qset());
        scheduler_add_task(new example_nv_vars());
    #endif

    /**
	 * Try the rx / tx tasks together to see how they queue data to each other.
	 */
    #if 0
        scheduler_add_task(new queue_tx());
        scheduler_add_task(new queue_rx());
    #endif

    /**
     * Another example of shared handles and producer/consumer using a queue.
     * In this example, producer will produce as fast as the consumer can consume.
     */
    #if 0
        scheduler_add_task(new producer());
        scheduler_add_task(new consumer());
    #endif

    /**
     * If you have RN-XV on your board, you can connect to Wifi using this task.
     * This does two things for us:
     *   1.  The task allows us to perform HTTP web requests (@see wifiTask)
     *   2.  Terminal task can accept commands from TCP/IP through Wifly module.
     *
     * To add terminal command channel, add this at terminal.cpp :: taskEntry() function:
     * @code
     *     // Assuming Wifly is on Uart3
     *     addCommandChannel(Uart3::getInstance(), false);
     * @endcode
     */
    #if 0
        Uart3 &u3 = Uart3::getInstance();
        u3.init(WIFI_BAUD_RATE, WIFI_RXQ_SIZE, WIFI_TXQ_SIZE);
        scheduler_add_task(new wifiTask(Uart3::getInstance(), PRIORITY_LOW));
    #endif

    scheduler_start(); ///< This shouldn't return
    return -1;
}
