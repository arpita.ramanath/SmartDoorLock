#include "uart3.hpp"
#include "stdio.h"
#include "LCD_uart.hpp"
#include "io.hpp"
#include "temperature_sensor.hpp"
/* Declare the variables to print on the screen below*/



void Uart_LCD()
{
    Uart3 &uart_3 = Uart3::getInstance();
    static int uart_temp = 1;
    if (uart_temp == 1)
    {
        uart_3.init(38400, 50, 50);
        uart_3.putChar(0xF0);
        uart_temp++;
    }


    uart_3.putline("$CLR_SCR"); //clear screen
    int i =TS.getFarenheit(); //get temp value
    vTaskDelay(200);
    char buf[4];
    sprintf(buf, "%d", i);
    uart_3.put("Temp :");
    uart_3.putline(buf);
    if((i>=70)&&(i<74))
    {
        uart_3.putline("It's hot outside");
    }
    else if(i>=74)
    {
        uart_3.putline("It's too hot outside, carry a hat");
    }
    else  if((i<70)&&(i>65))
    {

        uart_3.putline("It's cold outside");
    }
    else
    {

        uart_3.putline("It's too cold outside, carry a jacket");
    }


}







